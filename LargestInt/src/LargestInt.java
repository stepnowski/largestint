import java.util.Scanner;

public class LargestInt 
{
	private int largest;
	private Scanner input;
	
	public void determineLargest()
	{
		input = new Scanner(System.in);
		int count = 2;
		int current;
		
		System.out.println("This program will take 10 integers from you " + 
		"and find the largest integer");
		
		System.out.print("Please enter number 1:");
		current = input.nextInt();
		
		largest = current;
		
		while(count<=10)
		{
			System.out.printf("Please enter number %d:", count);
			current = input.nextInt();
						
			largest = Math.max(largest, current);
			count++;
		}
		
		System.out.printf("The largest number is: %d", largest);
	}
}
